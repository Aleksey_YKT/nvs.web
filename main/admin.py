from django.contrib import admin

from models import *

admin.site.register(Personal_Data)
admin.site.register(Speciality)
admin.site.register(Cabinet)
admin.site.register(Patient)
admin.site.register(Doctor)
admin.site.register(Polyclinic)
admin.site.register(Record)
admin.site.register(Time)
admin.site.register(Administrators)

