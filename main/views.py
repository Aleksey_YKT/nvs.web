# coding=utf-8

from django.http                    import HttpResponse, HttpResponseRedirect, HttpResponseForbidden, HttpResponseBadRequest
from django.shortcuts               import render, get_object_or_404, redirect
from django.contrib                 import auth
from django.contrib.auth.decorators import login_required
from django.views.decorators.csrf   import csrf_exempt
from django.core.mail               import send_mass_mail
from django.db                      import IntegrityError
from django.utils.http              import int_to_base36, base36_to_int
from django.utils.crypto            import salted_hmac
from django.contrib.auth            import authenticate, login, logout
from django.contrib.auth.models     import User
from django.utils                   import timezone

import math
import datetime
import time
import json

from main.models import *

def index_page(request):
    if request.user.is_authenticated():
        if is_admin(request):
            return redirect('admin_page')
        else:
            if is_doctor(request):
                return redirect('doctor_page')
            else:
                return redirect('patient_page')
    else:
        return redirect('login_page')

def login_page(request):
    if request.user.is_authenticated():
        if is_admin(request):
            return redirect('admin_page')
        else:
            if is_doctor(request):
                return redirect('doctor_page')
            else:
                return redirect('patient_page')
    else:
        return render(request, 'main/auth.html', locals())

# Login & Logout API
@csrf_exempt
def login_user(request):
    username = request.POST['login']
    password = request.POST['password']

    user = authenticate(username = username, password = password)
    if user is not None:
        if user.is_active:
            login(request, user)

            return redirect('index_page')
        else:
            return redirect('login_page')
    else:
        return redirect('login_page')

@csrf_exempt
def logout_user(request):
    logout(request)
    return redirect('login_page')


# mobile client API
@csrf_exempt
def login_mobile_user(request):
    error = {"message": "request method should be POST"}

    if request.method == "POST":
        username = request.POST.get('login', '')
        password = request.POST.get('password', '')

        result = {}

        user = authenticate(username = username, password = password)
        if user is not None:
            result["success"] = True
        else:
            return HttpResponseForbidden()

        return HttpResponse(json.dumps(result))

    return HttpResponse(json.dumps(error))

@csrf_exempt
def get_specialities_mobile(request):
    username = request.GET.get('login')
    password = request.GET.get('password')

    result = []

    user = authenticate(username = username, password = password)
    if user is not None:
        patient         = Patient.objects.get(patient_user = user)
        specialities    = Speciality.objects.filter(speciality_polyclinic = patient.polyclinic)

        for speciality in specialities:
            spec = {}

            spec["id"]      = speciality.id
            spec["name"]    = speciality.speciality_name

            result = result + [spec]
    else:
        return HttpResponseForbidden()

    return HttpResponse(json.dumps(result))

@csrf_exempt
def get_doctors_mobile(request):
    username        = request.GET.get('login')
    password        = request.GET.get('password')
    speciality_id   = request.GET.get('speciality')

    result = []

    user = authenticate(username = username, password = password)
    if user is not None:
        patient     = Patient.objects.get(patient_user = user)
        speciality  = Speciality.objects.get(id = int(speciality_id))
        doctors     = Doctor.objects.filter(speciality = speciality)

        for doctor in doctors:
            doc = {}

            doc["id"]               = doctor.id
            doc["surname"]          = doctor.personal_data.surname
            doc["name"]             = doctor.personal_data.name
            doc["patronymic"]       = doctor.personal_data.patronymic
            doc["cabinet"]          = doctor.cabinet.cabinet_number
            doc["cabinet_phone"]    = doctor.cabinet.cabinet_phone

            result = result + [doc]
    else:
        return HttpResponseForbidden()

    return HttpResponse(json.dumps(result))

@csrf_exempt
def get_times_mobile(request):
    username    = request.GET.get('login')
    password    = request.GET.get('password')
    doctor_id   = request.GET.get('doctor')

    result = []

    dthandler = lambda obj: obj.isoformat() if isinstance(obj, datetime.datetime) else None

    user = authenticate(username = username, password = password)
    if user is not None:
        patient = Patient.objects.get(patient_user = user)
        doctor  = Doctor.objects.get(id = int(doctor_id))
        times   = Time.objects.filter(doctor = doctor)

        for time in times:
            if time.available and time.date > timezone.now():
                t = {}

                t["id"]     = time.id
                t["date"]   = time.date

                result = result + [t]
    else:
        return HttpResponseForbidden()

    return HttpResponse(json.dumps(result, default = dthandler))

@csrf_exempt
def record_mobile_patient(request):
    error = {"message": "request method should be POST"}

    if request.method == "POST":
        username        = request.POST.get('login', '')
        password        = request.POST.get('password', '')
        time_id         = request.POST.get('time', '')
        doctor_id       = request.POST.get('doctor', '')

        result = {}

        user = authenticate(username = username, password = password)
        if user is not None:
            patient = Patient.objects.get(patient_user = user)
            time    = Time.objects.get(id = int(time_id))
            doctor  = Doctor.objects.get(id = int(doctor_id))

            record = Record()
            record.patient      = patient
            record.doctor       = doctor
            record.record_time  = time 

            time.available = False

            time.save()
            record.save()

            result["success"] = True
        else:
            return HttpResponseForbidden()

        return HttpResponse(json.dumps(result))

    return HttpResponse(json.dumps(error))

@csrf_exempt
def get_records_mobile(request):
    username = request.GET.get('login')
    password = request.GET.get('password')

    result = []

    dthandler = lambda obj: obj.isoformat() if isinstance(obj, datetime.datetime) else None

    user = authenticate(username = username, password = password)
    if user is not None:
        patient = Patient.objects.get(patient_user = user)
        records = Record.objects.filter(patient = patient)

        recs = []
        for record in records:
            if record.record_time.date > timezone.now():
                recs = recs + [record]

        for record in recs:
            rec = {}

            rec["id"]           = record.id
            rec["date"]         = record.record_time.date
            rec["doctor"]       = record.doctor.personal_data.surname + " " + record.doctor.personal_data.name + " " + record.doctor.personal_data.patronymic
            rec["speciality"]   = record.doctor.speciality.speciality_name
            rec["cabinet"]      = record.doctor.cabinet.cabinet_number
            rec["phone"]        = record.doctor.cabinet.cabinet_phone

            result = result + [rec]
    else:
        return HttpResponseForbidden()

    return HttpResponse(json.dumps(result, default = dthandler))

@csrf_exempt
def cancel_record_mobile(request):
    error = {"message": "request method should be POST"}

    if request.method == "POST":
        username    = request.POST.get('login', '')
        password    = request.POST.get('password', '')
        record_id   = request.POST.get('record', '')

        result = {}

        user = authenticate(username = username, password = password)
        if user is not None:
            record  = Record.objects.get(id = int(record_id))
            time    = record.record_time

            record.delete()

            time.available = True
            time.save()

            result["success"] = True
        else:
            return HttpResponseForbidden()

        return HttpResponse(json.dumps(result))

    return HttpResponse(json.dumps(error))

@csrf_exempt
def get_polyclinic_info_mobile(request):
    username = request.GET.get('login')
    password = request.GET.get('password')

    result = {}

    user = authenticate(username = username, password = password)
    if user is not None:
        patient = Patient.objects.get(patient_user = user)
        polyclinic = patient.polyclinic

        result["id"]                = polyclinic.id
        result["name"]              = polyclinic.name
        result["site"]              = polyclinic.site
        result["address_street"]    = polyclinic.address_street
        result["address_number"]    = polyclinic.address_number
        result["registry_phone"]    = polyclinic.registry_phone
        result["reception_phone"]   = polyclinic.reception_phone
        result["email"]             = polyclinic.email
    else:
        return HttpResponseForbidden()

    return HttpResponse(json.dumps(result))

@csrf_exempt
def get_doctors_info_mobile(request):
    username = request.GET.get('login')
    password = request.GET.get('password')

    result = []

    user = authenticate(username = username, password = password)
    if user is not None:
        patient = Patient.objects.get(patient_user = user)
        doctors = Doctor.objects.filter(polyclinic = patient.polyclinic)

        for doctor in doctors:
            doc = {}

            doc["id"]           = doctor.id
            doc["surname"]      = doctor.personal_data.surname 
            doc["name"]         = doctor.personal_data.name 
            doc["patronymic"]   = doctor.personal_data.patronymic 
            doc["speciality"]   = doctor.speciality.speciality_name
            doc["cabinet"]      = doctor.cabinet.cabinet_number
            doc["phone"]        = doctor.cabinet.cabinet_phone

            result = result + [doc]
    else:
        return HttpResponseForbidden()

    return HttpResponse(json.dumps(result))

@csrf_exempt
def get_patient_info_mobile(request):
    username = request.GET.get('login')
    password = request.GET.get('password')

    result = {}

    user = authenticate(username = username, password = password)
    if user is not None:
        patient = Patient.objects.get(patient_user = user)

        result["id"]                    = patient.id
        result["surname"]               = patient.personal_data.surname
        result["name"]                  = patient.personal_data.name
        result["patronymic"]            = patient.personal_data.patronymic
        result["passport_number"]       = patient.personal_data.passport_number
        result["date_of_birth"]         = patient.personal_data.date_of_birth
        result["residential_address"]   = patient.personal_data.residential_address
        result["phone_number"]          = patient.personal_data.phone_number
        result["policy_number"]         = patient.policy_number
        result["snils_number"]          = patient.snils_number
    else:
        return HttpResponseForbidden()

    return HttpResponse(json.dumps(result))

# Admin pages
def is_admin(request):
    if request.user.is_authenticated():
        if len(Administrators.objects.filter(administrator_user = request.user)) != 0:
            return True
        else:
            return False
    else:
        return False

def admin_page(request):
    if is_admin(request):
        return render(request, 'main/admin/admin_main.html', locals())
    else:
        return redirect('login_page')

def record_patient_page(request, speciality_id, doctor_id):
    if is_admin(request):
        admin               = Administrators.objects.get(administrator_user = request.user)
        selected_speciality = Speciality.objects.get(id = int(speciality_id))
        selected_doctor     = Doctor.objects.get(id = int(doctor_id))

        patients        = Patient.objects.filter(polyclinic = admin.polyclinic)
        specialities    = Speciality.objects.filter(speciality_polyclinic = admin.polyclinic)
        doctors         = Doctor.objects.filter(polyclinic = admin.polyclinic, speciality = selected_speciality)
        all_times       = Time.objects.filter(doctor = selected_doctor, available = True)
        times           = []

        for time in all_times:
            if time.date > timezone.now():
                times = times + [time]

        if selected_speciality not in specialities:
            speciality_id = specialities[0].id
            return redirect(record_patient_page, speciality_id, doctor_id)

        if selected_doctor not in doctors:
            doctor_id = doctors[0].id
            return redirect(record_patient_page, speciality_id, doctor_id)

        return render(request, 'main/admin/record_patient.html', {'selected_speciality': selected_speciality, 'selected_doctor': selected_doctor, 'patients': patients, 'specialities': specialities, 'doctors': doctors, 'times': times})
    else:
        return redirect('login_page')


def add_patient_page(request):
    if is_admin(request):
        return render(request, 'main/admin/add_patient.html', locals())
    else:
        return redirect('login_page')

def edit_patient_page(request, patient_id):
    if is_admin(request):
        admin = Administrators.objects.get(administrator_user = request.user)

        polyclinics         = Polyclinic.objects.all()
        patients            = Patient.objects.filter(polyclinic = admin.polyclinic)
        selected_patient    = Patient.objects.get(id = patient_id)

        return render(request, 'main/admin/edit_patient.html', {'patients': patients, 'selected_patient': selected_patient, 'polyclinics': polyclinics})
    else:
        return redirect('login_page')


def add_doctor_page(request):
    if is_admin(request):
        admin = Administrators.objects.get(administrator_user = request.user)

        specialities    = Speciality.objects.filter(speciality_polyclinic = admin.polyclinic)
        cabinets        = Cabinet.objects.filter(cabinet_polyclinic = admin.polyclinic)

        return render(request, 'main/admin/add_doctor.html', {'specialities': specialities, 'cabinets': cabinets})
    else:
        return redirect('login_page')

def edit_doctor_page(request, doctor_id):
    if is_admin(request):
        admin = Administrators.objects.get(administrator_user = request.user)

        specialities    = Speciality.objects.filter(speciality_polyclinic = admin.polyclinic)
        cabinets        = Cabinet.objects.filter(cabinet_polyclinic = admin.polyclinic)

        polyclinics     = Polyclinic.objects.all()
        doctors         = Doctor.objects.filter(polyclinic = admin.polyclinic)
        selected_doctor = Doctor.objects.get(id = doctor_id)

        return render(request, 'main/admin/edit_doctor.html', {'polyclinics': polyclinics, 'doctors': doctors, 'selected_doctor': selected_doctor, 'specialities': specialities, 'cabinets': cabinets})
    else:
        return redirect('login_page')


def add_cabinet_page(request):
    if is_admin(request):
        return render(request, 'main/admin/add_cabinet.html', locals())
    else:
        return redirect('login_page')

def edit_cabinet_page(request, cabinet_id):
    if is_admin(request):
        admin = Administrators.objects.get(administrator_user = request.user)

        cabinets            = Cabinet.objects.filter(cabinet_polyclinic = admin.polyclinic)
        selected_cabinet    = Cabinet.objects.get(id = cabinet_id)

        return render(request, 'main/admin/edit_cabinet.html', {'cabinets': cabinets, 'selected_cabinet': selected_cabinet})
    else:
        return redirect('login_page')


def add_speciality_page(request):
    if is_admin(request):
        return render(request, 'main/admin/add_speciality.html', locals())
    else:
        return redirect('login_page')

def edit_speciality_page(request, speciality_id):
    if is_admin(request):
        admin = Administrators.objects.get(administrator_user = request.user)

        specialities        = Speciality.objects.filter(speciality_polyclinic = admin.polyclinic)
        selecdet_speciality = Speciality.objects.get(id = speciality_id)

        return render(request, 'main/admin/edit_speciality.html', {'specialities': specialities, 'selecdet_speciality': selecdet_speciality})
    else:
        return redirect('login_page')


def edit_polyclinic_page(request):
    if is_admin(request):
        admin = Administrators.objects.get(administrator_user = request.user)
        polyclinic  = admin.polyclinic

        return render(request, 'main/admin/edit_polyclinic.html', {'polyclinic': polyclinic})
    else:
        return redirect('login_page')

# Admin API
@csrf_exempt
def record_patient(request, speciality_id, doctor_id):
    result = {}

    if is_admin(request):
        admin = Administrators.objects.get(administrator_user = request.user)

        patient_id  = request.POST["patient"]
        time_id     = request.POST["time"]

        patient = Patient.objects.get(id = int(patient_id))
        time    = Time.objects.get(id = int(time_id))
        doctor  = Doctor.objects.get(id = int(doctor_id))

        record = Record()
        record.patient      = patient
        record.doctor       = doctor
        record.record_time  = time

        time.available = False

        time.save()
        record.save()

        return redirect('admin_page')
    else:
        return redirect('login_page')

    return HttpResponse(json.dumps(result))

@csrf_exempt
def add_patient(request):
    result = {}

    if is_admin(request):
        admin = Administrators.objects.get(administrator_user = request.user)

        login                   = request.POST["login"]
        password                = request.POST["password"]
        surname                 = request.POST["surname"]
        name                    = request.POST["name"]
        patronymic              = request.POST["patronymic"]
        passport_number         = request.POST["passport_number"]
        residential_address     = request.POST["residential_address"]
        date_of_birth           = request.POST["date_of_birth"]
        policy_number           = request.POST["policy_number"]
        snils_number            = request.POST["snils_number"]
        phone_number            = request.POST["phone_number"]

        user = User.objects.create_user(login, '', password)

        patient_data                        = Personal_Data()
        patient_data.surname                = surname
        patient_data.name                   = name
        patient_data.patronymic             = patronymic
        patient_data.passport_number        = passport_number
        patient_data.date_of_birth          = date_of_birth
        patient_data.residential_address    = residential_address
        patient_data.phone_number           = phone_number

        patient_data.save()

        patient                 = Patient()
        patient.patient_user    = user
        patient.personal_data   = patient_data
        patient.polyclinic      = admin.polyclinic
        patient.policy_number   = policy_number
        patient.snils_number    = snils_number

        patient.save()
        return redirect('admin_page')
    else:
        return redirect('login_page')

    return HttpResponse(json.dumps(result))

@csrf_exempt
def edit_patient(request, patient_id):
    result = {}

    if is_admin(request):
        admin = Administrators.objects.get(administrator_user = request.user)

        surname                 = request.POST["surname"]
        name                    = request.POST["name"]
        patronymic              = request.POST["patronymic"]
        passport_number         = request.POST["passport_number"]
        residential_address     = request.POST["residential_address"]
        date_of_birth           = request.POST["date_of_birth"]
        policy_number           = request.POST["policy_number"]
        snils_number            = request.POST["snils_number"]
        phone_number            = request.POST["phone_number"]
        new_polyclinic          = request.POST["polyclinic"]

        polyclinic = Polyclinic.objects.get(name = new_polyclinic)

        patient                 = Patient.objects.get(id = patient_id)
        patient.polyclinic      = polyclinic
        patient.policy_number   = policy_number
        patient.snils_number    = snils_number

        patient_data                        = Personal_Data.objects.get(id = patient.personal_data.id)
        patient_data.surname                = surname
        patient_data.name                   = name
        patient_data.patronymic             = patronymic
        patient_data.passport_number        = passport_number
        patient_data.date_of_birth          = date_of_birth
        patient_data.residential_address    = residential_address
        patient_data.phone_number           = phone_number

        patient.save()
        patient_data.save()

        return redirect('admin_page')
    else:
        return redirect('login_page')

    return HttpResponse(json.dumps(result))

@csrf_exempt
def add_doctor(request):
    result = {}

    if is_admin(request):
        admin = Administrators.objects.get(administrator_user = request.user)

        login                   = request.POST["login"]
        password                = request.POST["password"]
        surname                 = request.POST["surname"]
        name                    = request.POST["name"]
        patronymic              = request.POST["patronymic"]
        passport_number         = request.POST["passport_number"]
        residential_address     = request.POST["residential_address"]
        date_of_birth           = request.POST["date_of_birth"]
        speciality_name         = request.POST["speciality"]
        cabinet_name            = request.POST["cabinet"]
        phone_number            = request.POST["phone_number"]

        user = User.objects.create_user(login, '', password)

        doctor_data                        = Personal_Data()
        doctor_data.surname                = surname
        doctor_data.name                   = name
        doctor_data.patronymic             = patronymic
        doctor_data.passport_number        = passport_number
        doctor_data.date_of_birth          = date_of_birth
        doctor_data.residential_address    = residential_address
        doctor_data.phone_number           = phone_number

        doctor_data.save()

        polyclinic_specialities = Speciality.objects.filter(speciality_polyclinic = admin.polyclinic, speciality_name = speciality_name)
        policlinic_cabinets     = Cabinet.objects.filter(cabinet_polyclinic = admin.polyclinic, cabinet_number = cabinet_name)

        doctor                  = Doctor()
        doctor.doctor_user      = user
        doctor.polyclinic       = admin.polyclinic
        doctor.personal_data    = doctor_data
        doctor.speciality       = polyclinic_specialities[0]
        doctor.cabinet          = policlinic_cabinets[0]

        doctor.save()
        return redirect('admin_page')
    else:
        return redirect('login_page')

    return HttpResponse(json.dumps(result))

@csrf_exempt
def edit_doctor(request, doctor_id):
    result = {}

    if is_admin(request):
        admin = Administrators.objects.get(administrator_user = request.user)

        surname                 = request.POST["surname"]
        name                    = request.POST["name"]
        patronymic              = request.POST["patronymic"]
        passport_number         = request.POST["passport_number"]
        residential_address     = request.POST["residential_address"]
        date_of_birth           = request.POST["date_of_birth"]
        speciality_name         = request.POST["speciality"]
        cabinet_name            = request.POST["cabinet"]
        phone_number            = request.POST["phone_number"]
        new_polyclinic          = request.POST["polyclinic"]

        polyclinic_specialities = Speciality.objects.filter(speciality_polyclinic = admin.polyclinic, speciality_name = speciality_name)
        policlinic_cabinets     = Cabinet.objects.filter(cabinet_polyclinic = admin.polyclinic, cabinet_number = cabinet_name)
        polyclinic = Polyclinic.objects.get(name = new_polyclinic)

        doctor                  = Doctor.objects.get(id = doctor_id)
        doctor.polyclinic       = polyclinic
        doctor.speciality       = polyclinic_specialities[0]
        doctor.cabinet          = policlinic_cabinets[0]

        doctor_data                         = Personal_Data.objects.get(id = doctor.personal_data.id)
        doctor_data.surname                 = surname
        doctor_data.name                    = name
        doctor_data.patronymic              = patronymic
        doctor_data.passport_number         = passport_number
        doctor_data.date_of_birth           = date_of_birth
        doctor_data.residential_address     = residential_address
        doctor_data.phone_number            = phone_number

        doctor.save()
        doctor_data.save()
        return redirect('admin_page')
    else:
        return redirect('login_page')

    return HttpResponse(json.dumps(result))

@csrf_exempt
def add_speciality(request):
    result = {}

    if is_admin(request):
        admin = Administrators.objects.get(administrator_user = request.user)

        name = request.POST["name"]

        speciality = Speciality()
        speciality.speciality_polyclinic    = admin.polyclinic
        speciality.speciality_name          = name

        speciality.save()

        return redirect('admin_page')
    else:
        return redirect('login_page')

    return HttpResponse(json.dumps(result))

@csrf_exempt
def edit_speciality(request, speciality_id):
    result = {}

    if is_admin(request):
        admin = Administrators.objects.get(administrator_user = request.user)

        speciality_name = request.POST["name"]

        speciality = Speciality.objects.get(id = speciality_id)
        speciality.speciality_polyclinic    = admin.polyclinic
        speciality.speciality_name          = speciality_name

        speciality.save()

        return redirect('admin_page')
    else:
        return redirect('login_page')

    return HttpResponse(json.dumps(result))

@csrf_exempt
def add_cabinet(request):
    result = {}

    if is_admin(request):
        admin = Administrators.objects.get(administrator_user = request.user)

        name    = request.POST["name"]
        phone   = request.POST["phone"]

        cabinet = Cabinet()
        cabinet.cabinet_polyclinic  = admin.polyclinic

        if name != "":
            cabinet.cabinet_number = name

        cabinet.cabinet_phone = phone

        cabinet.save()

        return redirect('admin_page')
    else:
        return redirect('login_page')

    return HttpResponse(json.dumps(result))

@csrf_exempt
def edit_cabinet(request, cabinet_id):
    result = {}

    if is_admin(request):
        admin = Administrators.objects.get(administrator_user = request.user)

        cabinet_name    = request.POST["name"]
        cabinet_phone   = request.POST["phone"]

        cabinet = Cabinet.objects.get(id = cabinet_id)
        cabinet.cabinet_polyclinic  = admin.polyclinic

        cabinet.cabinet_number  = cabinet_name
        cabinet.cabinet_phone   = cabinet_phone

        cabinet.save()

        return redirect('admin_page')
    else:
        return redirect('login_page')

    return HttpResponse(json.dumps(result))

@csrf_exempt
def edit_polyclinic(request):
    result = {}

    if is_admin(request):
        admin = Administrators.objects.get(administrator_user = request.user)

        name                = request.POST["name"]
        site                = request.POST["site"]
        address_street      = request.POST["address_street"]
        address_number      = request.POST["address_number"]
        registry_phone      = request.POST["registry_phone"]
        reception_phone     = request.POST["reception_phone"]
        email               = request.POST["email"]

        polyclinic  = admin.polyclinic

        polyclinic.name             = name
        polyclinic.site             = site
        polyclinic.address_street   = address_street
        polyclinic.address_number   = address_number
        polyclinic.registry_phone   = registry_phone
        polyclinic.reception_phone  = reception_phone
        polyclinic.email            = email

        polyclinic.save()

        return redirect('admin_page')
    else:
        return redirect('login_page')

    return HttpResponse(json.dumps(result))


# Doctor pages
def is_doctor(request):
    if request.user.is_authenticated():
        if len(Doctor.objects.filter(doctor_user = request.user)) != 0:
            return True
        else:
            return False
    else:
        return False

def doctor_page(request):
    if is_doctor(request):
        doctor = Doctor.objects.get(doctor_user = request.user)

        return render(request, 'main/doctor/doctor_main.html', {'doctor': doctor})
    else:
        return redirect('login_page')

def doctor_record_pacient(request):
    if is_doctor(request):
        doctor      = Doctor.objects.get(doctor_user = request.user)
        patients    = Patient.objects.filter(polyclinic = doctor.polyclinic)
        times       = Time.objects.filter(doctor = doctor, available = True)

        return render(request, 'main/doctor/record_patient.html', {'doctor': doctor, 'patients': patients, 'times': times})
    else:
        return redirect('login_page')

def doctor_records(request):
    if is_doctor(request):
        doctor = Doctor.objects.get(doctor_user = request.user)
        records = Record.objects.filter(doctor = doctor)

        return render(request, 'main/doctor/records.html', {'doctor': doctor, 'records': records})
    else:
        return redirect('login_page')

def doctor_add_schedule(request):
    if is_doctor(request):
        doctor = Doctor.objects.get(doctor_user = request.user)

        return render(request, 'main/doctor/add_schedule.html', {'doctor': doctor})
    else:
        return redirect('login_page')

def doctor_del_schedule(request):
    if is_doctor(request):
        doctor          = Doctor.objects.get(doctor_user = request.user)
        all_schedule    = Time.objects.filter(doctor = doctor)

        schedule = []
        for s in all_schedule:
            if s.date > timezone.now() and s.available == True:
                schedule = schedule + [s]

        return render(request, 'main/doctor/del_schedule.html', {'doctor': doctor, 'schedule': schedule})
    else:
        return redirect('login_page')


# Doctor API
@csrf_exempt
def record_doctor_pacient(request):
    if is_doctor(request):
        patient = request.POST['patient']
        time    = request.POST['time']

        doctor  = Doctor.objects.get(doctor_user = request.user)
        time    = Time.objects.get(id = str(time))

        record = Record()
        record.doctor       = doctor
        record.patient      = Patient.objects.get(id = str(patient))
        record.record_time  = time
        
        time.available  = False
        time.save()
        record.save()

        return redirect('doctor_page')
    else:
        return redirect('login_page')

@csrf_exempt
def add_doctor_schedule(request):
    if is_doctor(request):
        date            = request.POST['date']
        begin_time      = request.POST['begin_time']
        end_time        = request.POST['end_time']
        timeout_begin   = request.POST['timeout_begin']
        timeout_end     = request.POST['timeout_end']
        interval        = int(request.POST['interval'])

        hour    = int(begin_time.split(':')[0])
        minute  = int(begin_time.split(':')[1])

        end_hour    = int(end_time.split(':')[0])
        end_minute  = int(end_time.split(':')[1])

        timeout_begin_hour    = 99 
        timeout_begin_minute  = 99

        timeout_end_hour    = 99 
        timeout_end_minute  = 99 

        if timeout_begin != '' and timeout_end != '':
            timeout_begin_hour      = int(timeout_begin.split(':')[0])
            timeout_begin_minute    = int(timeout_begin.split(':')[1])

            timeout_end_hour    = int(timeout_end.split(':')[0])
            timeout_end_minute  = int(timeout_end.split(':')[1])


        absolute_time       = hour * 60 + minute
        absolute_end_time   = end_hour * 60 + end_minute

        absolute_timeout_begin  = timeout_begin_hour * 60 + timeout_begin_minute
        absolute_timeout_end    = timeout_end_hour * 60 + timeout_end_minute

        while absolute_time < absolute_end_time:
            if absolute_timeout_begin != 6039 and absolute_timeout_end != 6039 and absolute_time >= absolute_timeout_begin and absolute_time < absolute_timeout_end:
                pass
            else:
                time = Time()

                time_str    = date + ' ' + str(hour) + ':' + str(minute)
                new_time    = datetime.datetime.strptime(time_str, "%d.%m.%Y %H:%M")
                time.date   = new_time
                time.doctor = Doctor.objects.get(doctor_user = request.user)

                time.save()

            minute = int(minute) + interval
            if minute >= 60:
                minute = minute % 60
                hour = int(hour) + 1

            absolute_time = hour * 60 + minute
        return redirect('doctor_page')
    else:
        return redirect('login_page')

@csrf_exempt
def del_doctor_schedule(request):
    if is_doctor(request):
        selected_times = request.POST.getlist('selected_times')

        for time in selected_times:
            time = Time.objects.get(id = int(time))
            time.delete()

        return redirect('doctor_page')
    else:
        return redirect('login_page')


# Patient pages
def is_patient(request):
    if request.user.is_authenticated():
        if len(Patient.objects.filter(patient_user = request.user)) != 0:
            return True
        else:
            return False
    else:
        return False

def patient_page(request):
    if is_patient(request):
        patient = Patient.objects.get(patient_user = request.user)

        return render(request, 'main/patient/patient_main.html', {'patient': patient})
    else:
        return redirect('login_page')

def patient_record_page(request, speciality_id, doctor_id):
    if is_patient(request):
        patient             = Patient.objects.get(patient_user = request.user)

        specialities        = Speciality.objects.filter(speciality_polyclinic = patient.polyclinic)
        selected_speciality = Speciality.objects.get(id = int(speciality_id))

        doctors             = Doctor.objects.filter(speciality = selected_speciality)
        selected_doctor     = Doctor.objects.get(id = int(doctor_id))

        times               = Time.objects.filter(doctor = selected_doctor)
        available_times     = []
        for time in times:
            if time.available:
                available_times = available_times + [time]

        if selected_speciality not in specialities:
            speciality_id = specialities[0].id
            return redirect('patient_record_page', speciality_id, doctor_id)

        if selected_doctor not in doctors:
            doctor_id = doctors[0].id
            return redirect('patient_record_page', speciality_id, doctor_id)


        return render(request, 'main/patient/patient_record.html', {'patient': patient, 'specialities': specialities, 'selected_speciality': selected_speciality, 'doctors': doctors, 'selected_doctor': selected_doctor, 'times': available_times})
    else:
        return redirect('login_page')

def patient_records_page(request):
    if is_patient(request):
        patient = Patient.objects.get(patient_user = request.user)
        records = Record.objects.filter(patient = patient)

        expired_records = []
        future_records  = []

        for record in records:
            if record.record_time.date < timezone.now():
                expired_records = expired_records + [record]
            else:
                future_records = future_records + [record]
 
        return render(request, 'main/patient/patient_records.html', {'patient': patient, 'expired_records': expired_records, 'future_records': future_records})
    else:
        return redirect('login_page')

def cancel_patient_record_page(request):
    if is_patient(request):
        patient = Patient.objects.get(patient_user = request.user)
        all_records = Record.objects.filter(patient = patient)

        records = []

        for record in all_records:
            if record.record_time.date > timezone.now():
                records = records + [record]

        return render(request, 'main/patient/cancel_record.html', {'patient': patient, 'records': records})
    else:
        return redirect('login_page')

def polyclinic_info_page(request):
    if is_patient(request):
        patient     = Patient.objects.get(patient_user = request.user)
        polyclinic  = patient.polyclinic

        return render(request, 'main/patient/polyclinic_info.html', {'patient': patient, 'polyclinic': polyclinic})
    else:
        return redirect('login_page')

def doctors_info_page(request):
    if is_patient(request):
        patient = Patient.objects.get(patient_user = request.user)
        doctors = Doctor.objects.filter(polyclinic = patient.polyclinic) 

        return render(request, 'main/patient/doctors_info.html', {'patient': patient, 'doctors': doctors})
    else:
        return redirect('login_page')

def patient_info_page(request):
    if is_patient(request):
        patient = Patient.objects.get(patient_user = request.user)

        return render(request, 'main/patient/patient_info.html', {'patient': patient})
    else:
        return redirect('login_page')


# Patient API
@csrf_exempt
def patient_record(request, doctor_id):
    if is_patient(request):
        time_id     = request.POST['time']

        patient     = Patient.objects.get(patient_user = request.user)
        doctor      = Doctor.objects.get(id = int(doctor_id))
        time        = Time.objects.get(id = int(time_id))

        record = Record()
        record.doctor       = doctor
        record.patient      = patient
        record.record_time  = time
        
        time.available  = False
        time.save()
        record.save()

        return redirect('patient_page')
    else:
        return redirect('login_page')

@csrf_exempt
def cancel_patient_record(request):
    if is_patient(request):
        record_id     = request.POST['selected_records']

        patient     = Patient.objects.get(patient_user = request.user)
        record      = Record.objects.get(id = int(record_id))
        time        = record.record_time

        record.delete()
        
        time.available  = True
        time.save()

        return redirect('patient_page')
    else:
        return redirect('login_page')
