from django.conf.urls import url

from main import views

urlpatterns = [
    url(r'^$', views.index_page, name = 'index_page'),

    # login & logout API
    url(r'^login/$', views.login_page, name = 'login_page'),
    url(r'^login_user/$', views.login_user),
    url(r'^logout_user/$', views.logout_user),

    # mobile client API
    url(r'^api/mobile/login_user/$', views.login_mobile_user),
    url(r'^api/mobile/record_patient/get_specialities/$', views.get_specialities_mobile),
    url(r'^api/mobile/record_patient/get_doctors/$', views.get_doctors_mobile),
    url(r'^api/mobile/record_patient/get_times/$', views.get_times_mobile),
    url(r'^api/mobile/record_patient/record/$', views.record_mobile_patient),
    url(r'^api/mobile/get_records/$', views.get_records_mobile),
    url(r'^api/mobile/cancel_record/$', views.cancel_record_mobile),
    url(r'^api/mobile/get_polyclinic_info/', views.get_polyclinic_info_mobile),
    url(r'^api/mobile/get_doctors_info/', views.get_doctors_info_mobile),
    url(r'^api/mobile/get_patient_info/', views.get_patient_info_mobile),

    # admin pages
    url(r'^padmin/$', views.admin_page, name = 'admin_page'),
    url(r'^padmin/record_patient/(?P<speciality_id>[0-9]+)/(?P<doctor_id>[0-9]+)$', views.record_patient_page, name = 'record_patient_page'),
    url(r'^padmin/add_patient/$', views.add_patient_page),
    url(r'^padmin/edit_patient/(?P<patient_id>[0-9]+)$', views.edit_patient_page),
    url(r'^padmin/add_doctor/$', views.add_doctor_page),
    url(r'^padmin/edit_doctor/(?P<doctor_id>[0-9]+)$', views.edit_doctor_page),
    url(r'^padmin/add_cabinet/$', views.add_cabinet_page),
    url(r'^padmin/edit_cabinet/(?P<cabinet_id>[0-9]+)$', views.edit_cabinet_page),
    url(r'^padmin/add_speciality/$', views.add_speciality_page),
    url(r'^padmin/edit_speciality/(?P<speciality_id>[0-9]+)$', views.edit_speciality_page),
    url(r'^padmin/edit_polyclinic/$', views.edit_polyclinic_page),

    # admin API
    url(r'^api/record_patient/(?P<speciality_id>[0-9]+)/(?P<doctor_id>[0-9]+)$', views.record_patient),
    url(r'^api/add_patient/$', views.add_patient),
    url(r'^api/edit_patient/(?P<patient_id>[0-9]+)$', views.edit_patient),
    url(r'^api/add_doctor/$', views.add_doctor),
    url(r'^api/edit_doctor/(?P<doctor_id>[0-9]+)$', views.edit_doctor),
    url(r'^api/add_speciality/$', views.add_speciality),
    url(r'^api/edit_speciality/(?P<speciality_id>[0-9]+)$', views.edit_speciality),
    url(r'^api/add_cabinet/$', views.add_cabinet),
    url(r'^api/edit_cabinet/(?P<cabinet_id>[0-9]+)$', views.edit_cabinet),
    url(r'^api/edit_polyclinic/$', views.edit_polyclinic),


    # doctor pages
    url(r'^doctor/$', views.doctor_page, name = 'doctor_page'),
    url(r'^doctor/record_pacient/$', views.doctor_record_pacient),
    url(r'^doctor/records/$', views.doctor_records),
    url(r'^doctor/add_schedule/$', views.doctor_add_schedule),
    url(r'^doctor/del_schedule/$', views.doctor_del_schedule),

    # doctor API
    url(r'^api/record_doctor_pacient/$', views.record_doctor_pacient),
    url(r'^api/add_doctor_schedule/$', views.add_doctor_schedule),
    url(r'^api/del_doctor_schedule/$', views.del_doctor_schedule),


    # patient pages
    url(r'^patient/$', views.patient_page, name = 'patient_page'),
    url(r'^patient/record/(?P<speciality_id>[0-9]+)/(?P<doctor_id>[0-9]+)/$', views.patient_record_page, name = 'patient_record_page'),
    url(r'^patient/my_records/$', views.patient_records_page),
    url(r'^patient/cancel_record/$', views.cancel_patient_record_page),
    url(r'^patient/polyclinic_info/$', views.polyclinic_info_page),
    url(r'^patient/doctors_info/$', views.doctors_info_page),
    url(r'^patient/my_info/$', views.patient_info_page),

    # patient API
    url(r'^api/patient_record/(?P<doctor_id>[0-9]+)$', views.patient_record),
    url(r'^api/cancel_patient_record/$', views.cancel_patient_record),
]
