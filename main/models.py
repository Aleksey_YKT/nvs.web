# coding=utf-8

from django.db                  import models
from django.contrib.auth.models import User

class Personal_Data(models.Model):
    surname             = models.CharField(max_length = 100)
    name                = models.CharField(max_length = 100)
    patronymic          = models.CharField(max_length = 100)
    passport_number     = models.CharField(max_length = 100)
    date_of_birth       = models.CharField(max_length = 100)
    residential_address = models.CharField(max_length = 100)
    phone_number        = models.CharField(max_length = 100, blank = True)

    def __unicode__(self):
        return self.surname + ' ' + self.name + ' ' + self.patronymic

class Polyclinic(models.Model):
    name            = models.CharField(max_length = 100)
    site            = models.CharField(max_length = 100, blank = True)
    address_street  = models.CharField(max_length = 100)
    address_number  = models.CharField(max_length = 100)
    registry_phone  = models.CharField(max_length = 100, blank = True)
    reception_phone = models.CharField(max_length = 100, blank = True)
    email           = models.CharField(max_length = 100, blank = True)

    def __unicode__(self):
        return self.name

class Patient(models.Model):
    patient_user    = models.OneToOneField(User)
    personal_data   = models.ForeignKey(Personal_Data)
    polyclinic      = models.ForeignKey(Polyclinic)
    qr_code         = models.CharField(max_length = 1000, blank = True)
    policy_number   = models.CharField(max_length = 100)
    snils_number    = models.CharField(max_length = 100)

    def __unicode__(self):
        return self.personal_data.surname + ' ' + self.personal_data.name + ' ' + self.personal_data.patronymic

class Speciality(models.Model):
    speciality_polyclinic  = models.ForeignKey(Polyclinic)
    speciality_name = models.CharField(max_length = 100)

    def __unicode__(self):
        return self.speciality_name

class Cabinet(models.Model):
    cabinet_polyclinic  = models.ForeignKey(Polyclinic)
    cabinet_number      = models.CharField(max_length = 100)
    cabinet_phone       = models.CharField(max_length = 100, blank = True)

    def __unicode__(self):
        return self.cabinet_number

class Administrators(models.Model):
    administrator_user  = models.OneToOneField(User)
    name                = models.CharField(max_length = 100)
    polyclinic          = models.ForeignKey(Polyclinic)

    def __unicode__(self):
        return self.name

class Doctor(models.Model):
    doctor_user         = models.OneToOneField(User)
    personal_data       = models.ForeignKey(Personal_Data)
    polyclinic          = models.ForeignKey(Polyclinic)
    speciality          = models.ForeignKey(Speciality)
    cabinet             = models.ForeignKey(Cabinet)
    photo               = models.ImageField(upload_to = 'images', blank = True)

    def __unicode__(self):
        return self.speciality.speciality_name + ' - ' + self.personal_data.surname + ' ' + self.personal_data.name + ' ' + self.personal_data.patronymic

class Time(models.Model):
    date        = models.DateTimeField('')
    available   = models.BooleanField(default = True)
    doctor      = models.ForeignKey(Doctor)

    def __unicode__(self):
        return self.date.strftime("%d.%m.%Y %H:%M")

class Record(models.Model):
    patient     = models.ForeignKey(Patient)
    doctor      = models.ForeignKey(Doctor)
    record_time = models.ForeignKey(Time)